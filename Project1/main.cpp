#include <iostream>
#include <vector>
#include <iterator>
#include <array>

/*int getLength(void* parameter) {
	return sizeof(parameter) / sizeof(parameter[0]);
}*/
float calcVelocity(float x, float xOrigin, float t, float tOrigin) {
	float stage1A = x - xOrigin;
	float stage2A = t - tOrigin;
	std::cout << "Operation after substraction: ";
	std::cout << stage1A;
	std::cout << "/";
	std::cout << stage2A << std::endl;
	std::cout << "Result is: ";
	return stage1A / stage2A; // Return division of two substracts
}
int main() {
	std::cout << "Insert amount of required parameters by the function: ";
	int amountOfValues; std::cin >> amountOfValues;
	float *arrayOfValues = new float[amountOfValues];
	std::cout << "Got " << amountOfValues << std::endl;
	for (int c = 0; c < amountOfValues; c++) {
		std::cout << "Insert Value: ";
		std::cin >> arrayOfValues[c];
		//std::cout << std::endl;
	}
	std::cout << calcVelocity(arrayOfValues[0], arrayOfValues[1], arrayOfValues[2], arrayOfValues[3]) << std::endl;
	std::cin.get();
}